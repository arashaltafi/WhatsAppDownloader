package com.arash.altafi.whatsappdownloader.home

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.arash.altafi.whatsappdownloader.R
import com.arash.altafi.whatsappdownloader.utils.MyAlert
import com.arash.altafi.whatsappdownloader.utils.Utils
import com.arash.altafi.whatsappdownloader.utils.Utils.Companion.WHATSAPP_STATUSES_LOCATION
import com.arash.altafi.whatsappdownloader.utils.Utils.Companion.WHATSAPP_STATUSES_SAVED_LOCATION
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import org.apache.commons.io.comparator.LastModifiedFileComparator
import java.io.File
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{

    private companion object {
        const val EXTERNAL_STORAGE_PERMISSION_CODE: Int = 343
        private const val TYPE_VIDEO = 12
        private const val TYPE_IMAGE = 13
        private const val TYPE_SAVED = 15
        var type123 : Int ?= null
        class FetchFilesTask(activity: HomeActivity) : AsyncTask<Int, Int, ArrayList<File>>() {

            private val mRef: WeakReference<HomeActivity> = WeakReference(activity)

            override fun doInBackground(vararg p0: Int?): ArrayList<File>? {

                return mRef.get()?.fetchFiles(p0[0])

            }

            override fun onPostExecute(result: ArrayList<File>) {
                super.onPostExecute(result)
                mRef.get()?.statusAdapter?.addAll(result)

            }
        }
    }

    private lateinit var statusAdapter:StatusAdapter

    var appInstalled : Boolean ?= null
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toggle.drawerArrowDrawable.color = getColor(R.color.colorWhite)
        }
        else {
            toggle.drawerArrowDrawable.color = ContextCompat.getColor(this , R.color.colorWhite)
        }

        nav_view.setNavigationItemSelectedListener(this)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        statusAdapter = StatusAdapter(this)

        statusRcV.layoutManager = GridLayoutManager(this,2)
        statusRcV.adapter = statusAdapter

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askForPermission()
        }

        FetchFilesTask(this).execute(TYPE_IMAGE)

    }

    override fun onResume() {
        super.onResume()
        if (type123 == 1) {
            FetchFilesTask(this).execute(TYPE_IMAGE)
        }
        else if (type123 == 2) {
            FetchFilesTask(this).execute(TYPE_VIDEO)
        }
        else if (type123 == 3) {
            FetchFilesTask(this).execute(TYPE_SAVED)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun askForPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
            requestPermissions( arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_STORAGE_PERMISSION_CODE)
        }
    }

    fun fetchFiles(type: Int?): ArrayList<File> {

        var parentDir = File(Environment.getExternalStorageDirectory().toString()+WHATSAPP_STATUSES_LOCATION)

        if (type == TYPE_SAVED){

            parentDir = File(Environment.getExternalStorageDirectory().toString()+ WHATSAPP_STATUSES_SAVED_LOCATION)
        }

        val files: Array<File>?
        files = parentDir.listFiles()

        val fetchedFiles: ArrayList<File> = ArrayList()

        if (files != null) {

            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE)

            for (file in files) {

                if (Utils.isImageFile(this,file.path) && type == TYPE_IMAGE){
                    fetchedFiles.add(file)
                    type123 = 1
                }

                if (Utils.isVideoFile(this,file.path) && type == TYPE_VIDEO){
                    fetchedFiles.add(file)
                    type123 = 2
                }
                if (type == TYPE_SAVED){
                    fetchedFiles.add(file)
                    type123 = 3
                }


            }
        }

        return fetchedFiles
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else {
            if (doubleBackToExitPressedOnce) {
                finish()
            } else {
                this.doubleBackToExitPressedOnce = true
                Toast.makeText(this, "برای خروج از برنامه مجدد کلید برگشت را فشار دهید", Toast.LENGTH_SHORT).show()
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 4000)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_share -> {
                shareApp()
            }
            R.id.nav_other -> {
                othersApp()
            }
            R.id.nav_about -> {
                showAbout()
            }
            R.id.nav_rate -> {
                rateApp()
            }
            R.id.nav_help -> {
                showHelp()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){

            MyAlert.create(this)
                .setTitle(getString(R.string.no_permission))
                .setMessage(getString(R.string.require_read_write_external_storage_permissin))
                .setOkButton(getString(R.string.allow)) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", packageName, null))
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            askForPermission()
                        }
                    }
                }.setCancelButton(getString(R.string.exit)) {
                    finish()
                }.setCancelable(false)
            return

        }

        FetchFilesTask(this).execute(TYPE_IMAGE)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askForPermission()
        }
        when (item.itemId) {

            R.id.navigation_images -> {
                FetchFilesTask(this).execute(TYPE_IMAGE)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_video -> {
                FetchFilesTask(this).execute(TYPE_VIDEO)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_saved -> {
                FetchFilesTask(this).execute(TYPE_SAVED)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun isCoffeeInstall(context : Context, packageName : String): Boolean {
        val pm = context.packageManager
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            appInstalled = true
        }
        catch (e: Exception) {
            appInstalled = false
        }
        return appInstalled!!
    }

    private fun showHelp(){
        Handler(Looper.myLooper()!!).postDelayed(Runnable {
            MyAlert.create(this).setTitle(getString(R.string.help))
                .setMessage(getString(R.string.help_message)).setOkButton("بستن",null)
        },500)
    }

    private fun showAbout(){
        val about = "برنامه نویس : آرش الطافی\n"
        Handler(Looper.myLooper()!!).postDelayed(Runnable {
            MyAlert.create(this).setTitle(getString(R.string.about))
                .setMessage(about).setOkButton("ارتباط با ما", object : View.OnClickListener{
                    override fun onClick(p0: View?) {
                        val url = "https://arashaltafi.ir/"
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(url)
                        startActivity(intent)
                    }
                })
                .setCancelButton("بستن",null)
        },500)
    }

    private fun shareApp() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.share_app_text,"https://cafebazaar.ir/app/" + this.packageName))
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }

    private fun rateApp(){
        if (isCoffeeInstall(this , "com.farsitel.bazaar")) {
            val intent = Intent(Intent.ACTION_EDIT)
            intent.data = Uri.parse("bazaar://details?id=" + this.packageName)
            intent.setPackage("com.farsitel.bazaar")
            startActivity(intent)
        }
        else {
            Toast.makeText(this, "لطفا ابتدا برنامه کافه بازار را نصب نمایید", Toast.LENGTH_LONG).show()
        }
    }

    private fun othersApp() {
        if (isCoffeeInstall(this , "com.farsitel.bazaar")) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("bazaar://collection?slug=by_author&aid=arashaltafi")
            intent.setPackage("com.farsitel.bazaar")
            startActivity(intent)
        }
        else {
            Toast.makeText(this, "لطفا ابتدا برنامه کافه بازار را نصب نمایید", Toast.LENGTH_LONG).show()
        }
    }

}