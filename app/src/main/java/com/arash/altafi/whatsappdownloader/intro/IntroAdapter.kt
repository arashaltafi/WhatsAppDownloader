package com.arash.altafi.whatsappdownloader.intro

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arash.altafi.whatsappdownloader.R
import com.bumptech.glide.Glide

class IntroAdapter(val list : List<IntroModel>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_intro, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemList = list[position]
        Glide.with(holder.itemView.context).load(itemList.image).into(holder.imgIntro)
        holder.txtTitle.setText(itemList.title)
        holder.txtContent.setText(itemList.content)
    }

    override fun getItemCount(): Int = list.size

}

class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

    val imgIntro = itemView.findViewById<ImageView>(R.id.img_intro)
    val txtContent = itemView.findViewById<TextView>(R.id.txt_content_intro)
    val txtTitle = itemView.findViewById<TextView>(R.id.txt_title_intro)

}