package com.arash.altafi.whatsappdownloader.intro

data class IntroModel(val image : Int, val title : String, val content : String)