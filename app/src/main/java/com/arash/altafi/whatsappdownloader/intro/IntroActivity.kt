package com.arash.altafi.whatsappdownloader.intro

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.arash.altafi.whatsappdownloader.R
import com.arash.altafi.whatsappdownloader.home.HomeActivity
import com.arash.altafi.whatsappdownloader.utils.SharedPreferencesIntro
import kotlinx.android.synthetic.main.activity_intro.*
import java.util.*

class IntroActivity : AppCompatActivity() {

    private var introAdapter: IntroAdapter ?= null
    private val introModels: ArrayList<IntroModel> = ArrayList()
    private var preferencesIntro: SharedPreferencesIntro ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferencesIntro = SharedPreferencesIntro()

        preferencesIntro!!.sharedPreferencesIntro(this)

        if (!preferencesIntro!!.isFirst()) {
            val intent = Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
        else {
            setContentView(R.layout.activity_intro)
            setIntro()
            setUpIndicator()
            setCurrent(0)
            viewpager_intro.registerOnPageChangeCallback(object : OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    setCurrent(position)
                }
            })
            listener()
        }

    }

    private fun listener() {
        img_forward.setOnClickListener {
            if (viewpager_intro.currentItem + 1 < introAdapter!!.itemCount) {
                viewpager_intro.currentItem = viewpager_intro.currentItem + 1
            }
        }
        img_done.setOnClickListener {
            preferencesIntro!!.saveIntroInfo(false)
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
        img_prev.setOnClickListener {
            if (viewpager_intro.currentItem + 1 <= introAdapter!!.itemCount) {
                viewpager_intro.currentItem = viewpager_intro.currentItem - 1
            }
        }
    }

    private fun setIntro() {
        introModels.add(IntroModel(R.drawable.helper_1, "مرحله اول", getString(R.string.one)))
        introModels.add(IntroModel(R.drawable.helper_2, "مرحله دوم", getString(R.string.two)))
        introModels.add(IntroModel(R.drawable.helper_3, "مرحله سوم", getString(R.string.three)))
        introModels.add(IntroModel(R.drawable.helper_4, "نکته", getString(R.string.four)))
        introAdapter = IntroAdapter(introModels)
        viewpager_intro.adapter = introAdapter
    }

    private fun setUpIndicator() {
        val indicator = arrayOfNulls<ImageView>(introAdapter!!.itemCount)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicator.indices) {
            indicator[i] = ImageView(this)
            indicator[i]!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.shape_indicator_inactive))
            indicator[i]!!.layoutParams = layoutParams
            lnr_idicator.addView(indicator[i])
        }
    }

    private fun setCurrent(index: Int) {
        val child: Int = lnr_idicator.childCount
        for (i in 0 until child) {
            val imageView = lnr_idicator.getChildAt(i) as ImageView
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.shape_indicator_active))
            }
            else {
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.shape_indicator_inactive))
            }
        }
        when (index) {
            introAdapter!!.itemCount - 1 -> {
                img_forward.visibility = View.GONE
                img_done.visibility = View.VISIBLE
            }
            1 -> {
                img_prev.visibility = View.VISIBLE
                img_done.visibility = View.INVISIBLE
                img_forward.visibility = View.VISIBLE
            }
            2 -> {
                img_prev.visibility = View.VISIBLE
                img_done.visibility = View.INVISIBLE
                img_forward.visibility = View.VISIBLE
            }
            else -> {
                img_forward.visibility = View.VISIBLE
                img_prev.visibility = View.INVISIBLE
                img_done.visibility = View.INVISIBLE
            }
        }
    }

}