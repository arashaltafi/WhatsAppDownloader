package com.arash.altafi.whatsappdownloader.image

import android.os.Bundle
import android.view.ScaleGestureDetector
import androidx.appcompat.app.AppCompatActivity
import com.arash.altafi.whatsappdownloader.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_image_view.*
import kotlinx.android.synthetic.main.content_image_view.*
import uk.co.senab.photoview.PhotoView
import uk.co.senab.photoview.PhotoViewAttacher
import java.io.File

class ImageViewActivity : AppCompatActivity() {

    private var mScaleGestureDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1.0f
    private var mAttacher: PhotoViewAttacher? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
        setSupportActionBar(toolbar)

        mScaleGestureDetector = ScaleGestureDetector(this, ScaleListener())
        val imageFile = File(intent.getStringExtra("image"))

        mAttacher = PhotoViewAttacher(imageView)
        Glide.with(this).load(imageFile).into(imageView)

    }

    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            mScaleFactor *= scaleGestureDetector.scaleFactor
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f))
            imageView.scaleX = mScaleFactor
            imageView.scaleY = mScaleFactor
            return true
        }
    }

}
