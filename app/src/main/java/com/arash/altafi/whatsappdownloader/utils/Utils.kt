package com.arash.altafi.whatsappdownloader.utils

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.webkit.MimeTypeMap
import java.io.File
import java.util.*


class Utils {

    companion object {

        const val WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses"
        const val WHATSAPP_STATUSES_SAVED_LOCATION = "/statusSaver"

        fun isImageFile(context: Context, path: String): Boolean {

            val uri: Uri = Uri.parse(path)
            val mimeType: String?
            mimeType = if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
                val cr = context.contentResolver
                cr.getType(uri)
            } else {
                val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.lowercase(Locale.getDefault()))
            }
            return mimeType != null && mimeType.startsWith("image")
        }

        fun isVideoFile(context: Context, path: String): Boolean {

            val uri: Uri = Uri.parse(path)
            val mimeType: String?
            mimeType = if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
                val cr = context.contentResolver
                cr.getType(uri)
            } else {
                val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.lowercase(Locale.getDefault()))
            }
            return mimeType != null && mimeType.startsWith("video")
        }

        fun addToGallery(context: Context, f: File) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                val contentUri = Uri.fromFile(f)
                mediaScanIntent.data = contentUri
                context.sendBroadcast(mediaScanIntent)
            }
            else {
                context.sendBroadcast(
                    Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))
                )
            }
        }

        fun shareFile(context: Context, f: File , type : String) {

            val builder = VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val shareIntent = Intent(Intent.ACTION_SEND)
            if (f.exists()) {
                shareIntent.type = "$type/*"
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + f.absolutePath))
                context.startActivity(Intent.createChooser(shareIntent, "Share image"))
            }

        }

    }
}