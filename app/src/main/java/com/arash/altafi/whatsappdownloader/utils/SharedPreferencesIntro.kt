package com.arash.altafi.whatsappdownloader.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesIntro {

    private var preferences: SharedPreferences? = null

    fun sharedPreferencesIntro(context: Context) {
        preferences = context.getSharedPreferences("intro_whatsappDownloader", Context.MODE_PRIVATE)
    }

    fun saveIntroInfo(first: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean("first", first)
        editor.apply()
    }

    fun isFirst(): Boolean {
        return preferences!!.getBoolean("first", true)
    }

}